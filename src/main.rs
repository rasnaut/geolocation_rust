//use crate::IpCell;
use csv::Error;
use std::{str::FromStr, time::Instant};
mod ip_data_base;
mod my_reader;

fn main() -> Result<(), Error> {
    let binding = match std::env::args().nth(1) {
        Some(argument) => argument,
        None => {
            println!("Arguments not found. Data base not set");
            String::new()
        }
    };
    let db_file = binding.as_str();
    println!("READY");
    let mut database: Option<ip_data_base::IpDataBase> = None;
    loop {
        let mut incomming = String::new();
        std::io::stdin()
            .read_line(&mut incomming)
            .expect("Command unreadable");
        let command: Vec<&str> = incomming.split(' ').collect();
        match command[0].trim() {
            "LOAD" => {
                if command.len() > 1 {
                    let db_file = command[1].trim();
                    parse_database_csv(&mut database, db_file);
                    println!("OK");
                } else if !db_file.is_empty() {
                    parse_database_csv(&mut database, db_file);
                    println!("OK");
                } else {
                    println!("Please enter the csv file!");
                }
            }
            "LOOKUP" => {
                if command.len() > 1 {
                    let addr_u32 = match std::net::Ipv4Addr::from_str(command[1].trim()) {
                        Ok(addr) => addr.into(),
                        Err(_) => {
                            println!("invalid ip address");
                            continue;
                        }
                    };
                    match database {
                        Some(ref db) => match db.find_cell_by_ip(addr_u32) {
                            Some(cell) => cell.print_cell_data(),
                            None => println!("ip not found"),
                        },
                        None => println!("database empty!"),
                    }
                }
            }
            "EXIT" => {
                println!("Ok!");
                break;
            }
            _ => println!("error: Unknown command received"),
        }
    }

    Ok(())
}

fn parse_database_csv(database: &mut Option<ip_data_base::IpDataBase>, db_file: &str) {
    //let inst = Instant::now();
    *database = Some(crate::my_reader::parse_csv_short(db_file).unwrap());
    //println!("parse db took: {} ms", inst.elapsed().as_millis());
}
