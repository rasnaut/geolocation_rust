use std::{
    fs::File,
    io::{self, prelude::*},
};

use crate::ip_data_base;

pub struct _BufReader {
    reader: io::BufReader<File>,
}

impl _BufReader {
    pub fn _open(path: impl AsRef<std::path::Path>) -> io::Result<Self> {
        let file = File::open(path)?;
        let reader = io::BufReader::new(file);

        Ok(Self { reader })
    }

    pub fn _read_line<'buf>(
        &mut self,
        buffer: &'buf mut String,
    ) -> Option<io::Result<&'buf mut String>> {
        buffer.clear();

        self.reader
            .read_line(buffer)
            .map(|u| if u == 0 { None } else { Some(buffer) })
            .transpose()
    }
}

pub fn parse_csv_short(file_name: &str) -> io::Result<ip_data_base::IpDataBase> {
    let mut new_db = ip_data_base::IpDataBase::new();
    let file = File::open(file_name)?;
    let reader = std::io::BufReader::new(file);
    // for line in reader.lines() {
    //     line.unwrap();
    // }

    let mut rdr = csv::Reader::from_reader(reader);

    for result in rdr.records() {
        let record = result?;
        let new_cell = ip_data_base::IpCell::new_from_string_record(record);
        new_db.add_cell(new_cell);
    }

    Ok(new_db)
}

pub fn _parse_csv(file_name: &str) -> io::Result<ip_data_base::IpDataBase> {
    let mut reader = _BufReader::_open(file_name)?;
    let mut buffer = String::new();
    let mut new_db = ip_data_base::IpDataBase::new();

    while let Some(line) = reader._read_line(&mut buffer) {
        let mut rdr = csv::Reader::from_reader(line?.as_bytes());
        for result in rdr.records() {
            let record = result?;
            let new_cell = ip_data_base::IpCell::new_from_string_record(record);
            new_db.add_cell(new_cell);
        }
    }
    Ok(new_db)
}
