#[derive(Clone)]
pub struct IpCell {
    pub start_range: u32,
    pub end_range: u32,
    pub country_code: String,
    pub country: String,
    pub state: String,
    pub city: String,
    pub latitude: String,
    pub longitude: String,
}

impl IpCell {
    pub fn in_range(&self, ip: u32) -> i64 {
        if self.start_range <= ip && ip <= self.end_range {
            return 0;
        } else if self.start_range > ip {
            return i64::from(ip) - i64::from(self.start_range);
        }

        i64::from(ip) - i64::from(self.end_range)
    }

    pub fn print_cell_data(&self) {
        println!("{},{}", self.country_code, self.city);
    }

    pub fn new_from_string_record(record: csv::StringRecord) -> Self {
        IpCell {
            start_range: record[0].parse().unwrap(),
            end_range: record[1].parse().unwrap(),
            country_code: record[2].to_string(),
            country: record[3].to_string(),
            state: record[4].to_string(),
            city: record[5].to_string(),
            latitude: record[6].to_string(),
            longitude: record[7].to_string(),
        }
    }
}

pub struct IpDataBase {
    data_base: Vec<IpCell>,
}

impl IpDataBase {
    pub fn new() -> Self {
        Self {
            data_base: Vec::new(),
        }
    }

    pub fn add_cell(&mut self, new_cell: IpCell) {
        self.data_base.push(new_cell);
    }
    pub fn _has_value(&self) -> bool {
        !self.data_base.is_empty()
    }

    pub fn binary_search_cell(&self, m_slice: &[IpCell], ip: u32) -> Option<IpCell> {
        let mid: usize;
        if m_slice.len() > 4 {
            mid = m_slice.len() / 2;

            let result = m_slice[mid].in_range(ip);
            match result {
                0 => return Some(m_slice[mid].clone()),
                d if d > 0 => {
                    return self.binary_search_cell(&m_slice[(mid + 1)..m_slice.len()], ip)
                }
                _ => return self.binary_search_cell(&m_slice[0..mid], ip),
            }
        } else if !m_slice.is_empty() {
            for itr in m_slice {
                let result = itr.in_range(ip);
                if result == 0 {
                    return Some(itr.clone());
                }
            }
        }
        None
    }

    pub fn find_cell_by_ip(&self, ip: u32) -> Option<IpCell> {
        self.binary_search_cell(&self.data_base[..], ip)
    }

    pub fn _print_db_data(&self) {
        for i in &self.data_base {
            i.print_cell_data();
        }
    }
}

impl Default for IpDataBase {
    fn default() -> Self {
        Self::new()
    }
}
